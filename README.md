# Support

Generate a csv file with the signer balances between dates.

By default the dates are:

* START_DATE -> 2019-11-01
* END_DATE -> current date

This values can be changed in this file `run`

## Scritps

To run the script and generate the file 

    $ source run

or

    $ ./run
