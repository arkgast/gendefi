import * as config from 'config';
import { get } from './http';

const fetchSignerBalance = (signer: string) => {
  const coreUrl = config.get('coreUrl');
  const url = `${coreUrl}/address/${signer}/outputs/unspent?counter=wd7VoAD3PzRdRRuKUbSUzL2gFgSD4Z8HRC`;
  return get(url);
};

const mem = new Map();
const getBalance = async (signer: string) => {
  if (!signer) return 0;
  if (mem.has(signer)) return 0;

  const { data } = await fetchSignerBalance(signer);
  const [output] = data.data.outputs;

  if (output) {
    const balance = Number.parseInt(output.content.balance.net);
    mem.set(signer, balance);
    return balance;
  }

  return 0;
};

export { getBalance };
