const getFileHeader = () => `BANK,SIGNER,BALANCE,TYPE,STATUS,AMOUNT,CREATED\n`;

const getFileRow = (tx: any, balance: number, sourceOrTarget: string) => {
  const bank = tx[`${sourceOrTarget}Bank`];
  const signer = tx[`${sourceOrTarget}`];
  const { type, status, amount, created } = tx;
  return `${bank},${signer},${balance},${type},${status},${amount},${created}\n`;
};

export { getFileHeader, getFileRow };
