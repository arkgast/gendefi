import axios from 'axios';

const { JWT } = process.env;

const get = (url: string) =>
  axios.get(url, { headers: { authorization: `Bearer ${JWT}` } });

export { get };
