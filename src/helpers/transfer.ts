import * as _ from 'lodash';
import * as config from 'config';

import { get } from './http';

const getQuery = (filters: any) =>
  `type=SEND,REQUEST&status=${filters.status}&created={"gt":"${filters.startDate}","lt":"${filters.endDate}"}&pagesize=${filters.pageSize}&sortBy=created`;

const fetchTransfers = (filters: any) => {
  const apiUrl = config.get('apiUrl');
  const query = getQuery(filters);
  const url = `${apiUrl}/v1/transfer?${query}`;

  return get(url);
};

const getTransfers = async (filters: any) => {
  const { data } = await fetchTransfers(filters);
  return data.entities.map((tx: any) => {
    const [source, target] = getSignersFromSnapshot(tx.snapshot);
    return {
      amount: tx.amount,
      status: tx.status,
      created: tx.created,
      source,
      sourceBank: tx.sourceBank,
      target,
      targetBank: tx.targetBank,
      type: tx.type,
    };
  });
};

const getSignersFromSnapshot = (snapshot: any) => {
  const sourceSigner = _.get(snapshot, 'source.signer.handle', '');
  const targetSigner = _.get(snapshot, 'target.signer.handle', '');
  return [sourceSigner, targetSigner];
};

export { getTransfers };
