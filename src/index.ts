require('dotenv').config();

import * as fs from 'fs';
import * as config from 'config';
import { from, of, zip } from 'rxjs';

import { filter, flatMap, distinct, map } from 'rxjs/operators';

import * as fileFormat from './helpers/file-format';
import { getTransfers } from './helpers/transfer';
import { getBalance } from './helpers/balance';

const FILTERS = config.get('defaultFilters');

const logProgress = (
  transfers: any,
  tx: any,
  sourceBalance: any,
  targetBalance: any,
  idx: any
) =>
  console.log({
    process: `${idx} - ${transfers.length} - ${tx.created}`,
    source: tx.source,
    sourceBalance,
    target: tx.target,
    targetBalance,
  });

export const createDesbalancesFile = async (filters: any = FILTERS) => {
  const transfers = await getTransfers(filters);
  const file = fs.createWriteStream(
    `${filters.startDate}-${filters.endDate}-${filters.status}.csv`
  );

  const fileHeader = fileFormat.getFileHeader();
  file.write(fileHeader);

  let idx = 0;
  for (const tx of transfers) {
    idx += 1;
    const [sourceBalance, targetBalance] = await Promise.all([
      getBalance(tx.source),
      getBalance(tx.target),
    ]);

    logProgress(transfers, tx, sourceBalance, targetBalance, idx);

    if (sourceBalance > 0) {
      const fileRow = fileFormat.getFileRow(tx, sourceBalance, 'source');
      file.write(fileRow);
    }

    if (targetBalance > 0) {
      const fileRow = fileFormat.getFileRow(tx, targetBalance, 'target');
      file.write(fileRow);
    }
  }
};
